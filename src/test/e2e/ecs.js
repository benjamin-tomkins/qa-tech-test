const centrePoint = require('./utils');

module.exports = {
  'Open browser': (browser) => {
    browser
      .windowMaximize()
      .url('http://localhost:3000/')
      .waitForElementVisible('body', 1000);
  },
  'Click button': (browser) => {
    browser
      .click('xpath', '//span[contains(text(),"Render the Challenge")]')
      .waitForElementVisible('tbody', 1000);
  },
  'Get and set first value': (browser) => {
    browser
      .useXpath()
      .getText('//tbody/tr[1]', (result) => {
        browser.setValue('//input[@data-test-id="submit-1"]', centrePoint(result));
      });
  },
  'Get and set second value': (browser) => {
    browser
      .useXpath()
      .getText('//tbody/tr[2]', (result) => {
        browser.setValue('//input[@data-test-id="submit-2"]', centrePoint(result));
      });
  },
  'Get and set third value': (browser) => {
    browser
      .useXpath()
      .getText('//tbody/tr[3]', (result) => {
        browser.setValue('//input[@data-test-id="submit-3"]', centrePoint(result));
      });
  },
  'Set name': (browser) => {
    browser.setValue('//input[@data-test-id="submit-4"]', 'Ben Tomkins');
  },
  'Submit answers': (browser) => {
    browser
      .useXpath()
      .click('//span[contains(text(),"Submit Answer")]')
      .waitForElementVisible('//div[contains(text(),"✅")]', 4000)
      .click('//span[contains(text(),"Close")]');
  },
  'Browser quit': (browser) => {
    browser
      .pause(5000)
      .end();
  },
};
