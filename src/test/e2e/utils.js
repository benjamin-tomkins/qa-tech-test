const add = (a, b) => a + b;

const centrePoint = (result) => {
  const arr = result.value.split(' ').map(Number);
  let i = 1;

  while (arr.slice(0, i).reduce(add) - arr.slice(i + 1).reduce(add)) {
    i += 1;
    if (i > arr.length - 2) return null;
  }
  return i;
};

module.exports = centrePoint;
