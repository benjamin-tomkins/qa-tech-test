### ECSD Tech Test

Candidate :  Ben Tomkins (Makers)

This test was coded using Nightwatch.js and the chrome driver on a mac

### Running the application

#### To run the Local Version of the App
You will need to have [node] and [yarn] both installed on your machine to run the app.

- Run : `git clone https://benjamin-tomkins@bitbucket.org/benjamin-tomkins/qa-tech-test.git`
- Run : `cd qa-tech-test/`
- Run : `npm install`
- Run : `npm start`

#### To run the tests, open another terminal window

- Run : `node nightwatch -e chrome`

#### Next step

The solution can be containerized to make the solution more rubust and deployable
